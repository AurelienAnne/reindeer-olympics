package com.aan.domain;

public class RacingReindeer {

    private Reindeer reindeer;
    private Integer distanceTraveled;
    private Boolean isFlying;
    private Integer stateDuration;

    public RacingReindeer(Reindeer reindeer, Integer distanceTraveled, Boolean isFlying, Integer stateDuration) {
        this.reindeer = reindeer;
        this.distanceTraveled = distanceTraveled;
        this.isFlying = isFlying;
        this.stateDuration = stateDuration;
    }

    public Reindeer getReindeer() {
        return reindeer;
    }

    public void setReindeer(Reindeer reindeer) {
        this.reindeer = reindeer;
    }

    public Integer getDistanceTraveled() {
        return distanceTraveled;
    }

    public void setDistanceTraveled(Integer distanceTraveled) {
        this.distanceTraveled = distanceTraveled;
    }

    public void addToDistanceTraveled(Integer addedDistance) {
        this.distanceTraveled += addedDistance;
    }

    public Boolean isFlying() {
        return isFlying;
    }

    public void setFlying(Boolean flying) {
        isFlying = flying;
    }

    public Integer getStateDuration() {
        return stateDuration;
    }

    public void setStateDuration(Integer stateDuration) {
        this.stateDuration = stateDuration;
    }

    public void removeOneSecond() {
        this.stateDuration -= 1;
    }

    @Override
    public String toString() {
        return getReindeer().getName() + " a parcouru " + getDistanceTraveled() + "km.";
    }
}
