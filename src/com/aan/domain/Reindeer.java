package com.aan.domain;

/**
 * Objet Reindeer
 */
public class Reindeer {
    private String name;
    private Integer speed;
    private Integer flyDuration;
    private Integer restDuration;

    public Reindeer(String name, Integer speed, Integer flyDuration, Integer restDuration) {
        this.name = name;
        this.speed = speed;
        this.flyDuration = flyDuration;
        this.restDuration = restDuration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Integer getFlyDuration() {
        return flyDuration;
    }

    public void setFlyDuration(Integer flyDuration) {
        this.flyDuration = flyDuration;
    }

    public Integer getRestDuration() {
        return restDuration;
    }

    public void setRestDuration(Integer restDuration) {
        this.restDuration = restDuration;
    }

    @Override
    public String toString() {
        return getName() + " peut voler à " + getSpeed() + " km/s pendant " + getFlyDuration() + " secondes," +
                " puis doit se reposer pendant " + getRestDuration() + " secondes.";
    }
}
