package com.aan.service;

import com.aan.domain.RacingReindeer;
import com.aan.domain.Reindeer;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class ReindeerService {

    /**
     * Réalisation de la course
     *
     * @param raceDuration durée de la course
     * @return Renne gagnant
     */
    public List<RacingReindeer> doTheRace(Integer raceDuration) {

        // Récupération des infos des rennes
        List<Reindeer> reindeerList = this.readReindeersInfosFile();

        // Création de la course
        List<RacingReindeer> racingReindeerList = new ArrayList<>();
        reindeerList.forEach(reindeer -> racingReindeerList.add(new RacingReindeer(reindeer, 0, true, reindeer.getFlyDuration())));

        // Réalisation de la course
        for (int i = 0; i < raceDuration; i++) {
            racingReindeerList.forEach(this::advanceOneSecond);
        }

        // Calcul des résultats
        racingReindeerList.sort(Comparator.comparing(RacingReindeer::getDistanceTraveled).reversed());
        List<RacingReindeer> winners = new ArrayList<>();
        // Le gagnant
        winners.add(racingReindeerList.get(0));
        Integer distance = racingReindeerList.get(0).getDistanceTraveled();
        // Possibles égalités
        for (int i = 1; i < racingReindeerList.size(); i++) {
            if (racingReindeerList.get(i).getDistanceTraveled().equals(distance)) {
                winners.add(racingReindeerList.get(1));
            } else {
                break;
            }
        }
        return winners;
    }

    /**
     * Lecture du fichier de ressources
     *
     * @return la liste des rennes
     */
    private List<Reindeer> readReindeersInfosFile() {
        List<Reindeer> reindeerList = new ArrayList<>();

        try {
            File file;
            URL resource = getClass().getClassLoader().getResource("reindeer-olympics.txt");
            if (resource == null) {
                throw new FileNotFoundException("file not found!");
            } else {
                file = new File(resource.toURI());
            }
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String[] data = myReader.nextLine().split(" ");
                String name = data[0];
                Integer speed = Integer.valueOf(data[3]);
                Integer flyDuration = Integer.valueOf(data[6]);
                Integer restDuration = Integer.valueOf(data[13]);
                reindeerList.add(new Reindeer(name, speed, flyDuration, restDuration));
            }
            myReader.close();

        } catch (FileNotFoundException | URISyntaxException e) {
            System.out.println("Fichier reindeer-olympics.txt non trouvé.");
            e.printStackTrace();
        }

        return reindeerList;
    }

    /**
     * Changer l'état du renne pour une seconde de course
     *
     * @param racingReindeer rennee en course
     */
    public void advanceOneSecond(RacingReindeer racingReindeer) {
        // Changement d'état
        if (racingReindeer.getStateDuration() <= 0) {
            // Si le rennes vole, il se repose, et inversement
            if (racingReindeer.isFlying()) {
                racingReindeer.setFlying(false);
                racingReindeer.setStateDuration(racingReindeer.getReindeer().getRestDuration());
            } else {
                racingReindeer.setFlying(true);
                racingReindeer.setStateDuration(racingReindeer.getReindeer().getFlyDuration());
            }
        }

        // Parcours de la distance s'il vole
        if (racingReindeer.isFlying()) {
            racingReindeer.addToDistanceTraveled(racingReindeer.getReindeer().getSpeed());
        }
        // Passage de la seconde
        racingReindeer.removeOneSecond();
    }
}
