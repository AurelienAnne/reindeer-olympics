package com.aan.main;

import com.aan.domain.RacingReindeer;
import com.aan.service.ReindeerService;

import java.util.List;

public class Main {

    private static final ReindeerService reindeerService = new ReindeerService();

    /**
     * Classe principale du programme
     *
     * @param args arguments du programme
     */
    public static void main(String[] args) {
        if (args == null || args.length != 1) {
            System.out.println("Utilisation du programme Reindeer Olympics : java Main.java <raceDuration>");
        } else {
            Integer raceDuration = Integer.valueOf(args[0]);
            List<RacingReindeer> winners = reindeerService.doTheRace(raceDuration);
            if (winners != null && winners.size() == 1) {
                System.out.println("Le renne gagnant au bout de " + raceDuration + " secondes est : " +
                        winners.get(0).getReindeer().getName() + ". Il a parcouru " +
                        winners.get(0).getDistanceTraveled() + " km.");
            } else if (winners != null && winners.size() > 1) {
                System.out.print("Après " + raceDuration + " secondes, égalité entre");
                for (RacingReindeer reindeer : winners) {
                    System.out.print(" " + reindeer.getReindeer().getName());
                }
                System.out.print(". Ils ont parcouru " + winners.get(0).getDistanceTraveled() + " km.");
            } else {
                System.out.println("La course ne s'est pas déroulée comme prévu, il n'y a pas de vainqueur.");
            }
        }
    }
}
