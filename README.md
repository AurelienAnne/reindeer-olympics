# Reindeer Olympics #

Programme de courses de rennes

### Lancement du programme ###

Importez ce repository dans votre environnement de développment.

Editez les RunConfigurations pour ajouter un argument : la durée de la course.

Buildez, puis lancez Main.java.

Le résultat de la course sera affiché dans la console.

(Les statistiques des rennes sont dans le fichier resources/reindeer-olympics.txt)