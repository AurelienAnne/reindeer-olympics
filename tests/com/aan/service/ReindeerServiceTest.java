package com.aan.service;

import com.aan.domain.RacingReindeer;
import com.aan.domain.Reindeer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReindeerServiceTest {

    ReindeerService reindeerService = new ReindeerService();

    @Test
    void advanceOneSecondFlying() {
        // Arrange
        Reindeer reindeer = new Reindeer("name", 15, 42, 42);
        RacingReindeer racingReindeer = new RacingReindeer(reindeer, 0, true, 42);
        // Act
        reindeerService.advanceOneSecond(racingReindeer);

        // Assert
        assertEquals(15, racingReindeer.getDistanceTraveled());
        assertTrue(racingReindeer.isFlying());
        assertEquals(41, racingReindeer.getStateDuration());
    }

    @Test
    void advanceOneSecondResting() {
        // Arrange
        Reindeer reindeer = new Reindeer("name", 15, 42, 42);
        RacingReindeer racingReindeer = new RacingReindeer(reindeer, 0, false, 42);
        // Act
        reindeerService.advanceOneSecond(racingReindeer);

        // Assert
        assertEquals(0, racingReindeer.getDistanceTraveled());
        assertFalse(racingReindeer.isFlying());
        assertEquals(41, racingReindeer.getStateDuration());
    }

    @Test
    void advanceOneSecondChangeState() {
        // Arrange
        Reindeer reindeer = new Reindeer("name", 15, 42, 42);
        RacingReindeer racingReindeer = new RacingReindeer(reindeer, 0, false, 0);
        // Act
        reindeerService.advanceOneSecond(racingReindeer);

        // Assert
        assertEquals(15, racingReindeer.getDistanceTraveled());
        assertTrue(racingReindeer.isFlying());
        assertEquals(41, racingReindeer.getStateDuration());
    }
}